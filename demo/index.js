// promise

// pendding : chờ
// resolve : thành công
// reject : thất bại

var foodArr = [];
axios({
  url: "https://63b2c99f5901da0ab36dbaed.mockapi.io/food",
  method: "GET",
})
  .then(function (res) {
    // tự động được gọi khi thành công
    foodArr = res.data;
    console.log(`  🚀: foodArr line 14`, foodArr);
  })
  .catch(function (err) {
    // tự động được gọi khi thất bại
    console.log(`  🚀: err`, err);
  });
console.log("foodArr - line 18", foodArr);

// async
// đồng bộ vs bất đồng bộ
// event loop

setTimeout(function () {
  console.log("hello in setTimeout 4s");
}, 4000);

setTimeout(function () {
  console.log("hello in setTimeout 2s");
}, 2000);

setTimeout(function () {
  console.log("yes");
}, 0);

console.log("no");
